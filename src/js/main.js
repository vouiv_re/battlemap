"use strict";

const main = document.getElementById("main");
const generateBtn = document.getElementById("generate-btn");
const sizeMap = 5;
let cellSelected = false;
let round = 16;
let position = [[25, 79], [79-round, 79-round], [79, 25], [79-round, -29+round], [25, -29], [-29+round, -29+round], [-29, 25], [-29+round, 79-round]];
let width = 50;

generateBtn.addEventListener("click", () => {
    main.innerHTML = "";
    for (let i = 0; i < sizeMap; i++) {
        let row = document.createElement("div");
        row.setAttribute("class", "row");
        for (let y = 0; y < sizeMap; y++) {
            let cell = document.createElement("div");
            cell.setAttribute("class", "cell");
            cell.setAttribute("location", `${i}${7}`);
            cell.addEventListener("click", (e) => {
                if (e.target.classList[1] === "selected") {
                    e.target.classList.remove("selected");
                    e.target.innerHTML = "";
                    cellSelected = false;
                } else if (cellSelected === false) {
                    e.target.classList.add("selected");
                    cellSelected = true;
                    for (let i = 0; i < 8; i++) {
                        let div = document.createElement("div");
                        let tab = document.createElement("div");
                        tab.setAttribute("class", "tab");
                        div.style.top = `${position[i][0]}px`
                        div.style.left = `${position[i][1]}px`
                        tab.style.width = `${width}px`
                        tab.style.height = `${width}px`
                        div.appendChild(tab);
                        e.target.appendChild(div);
                    }
                }
            }, false);
            row.appendChild(cell);
        }
        main.appendChild(row);
    }
}, false);
